#!/bin/bash

# test.sh
# Tests the endfloat package

# Author: Axel Sommerfeldt (axel.sommerfeldt@f-m.fm)
# URL:    https://gitlab.com/axelsommerfeldt/endfloat
# Date:   2020-12-16

source ./test-lib.sh

# Overwrite copy_package_files() since we don't have a "source" or "tex" folder here.
function copy_package_files
{
  cp -a "$basedir"/endfloat*.sty "$basedir/efxmpl.cfg" .
}

disable test/test_errors.tex

main "$@"

