#!/bin/sh
#
# 2011-12-12: 1st version for the endfloat package
# 2011-12-23: "*.cfg" changed to "efxmpl.cfg"
# 2018-03-24: Adapted to current version of ctanify
# 2020-07-29: ltxdoc.cfg removed from created archive

set -e
dist_dir=$(pwd)
temp_dir="/tmp/endfloat"

rm -fr "$temp_dir"
mkdir -p "$temp_dir/v2.4"
cp README COPYING endfloat.dtx endfloat.ins endfloat.pdf efxmpl.cfg endfloat*.sty "$temp_dir"
cp v2.4/endfloat.dtx "$temp_dir/v2.4"
cd "$temp_dir"

# shellcheck disable=SC2035
if ctanify --noauto \
     README "COPYING=doc/latex/endfloat" \
     *.dtx *.ins *.pdf \
     "v2.4/*.dtx=source/latex/endfloat/v2.4" \
     --tdsonly "*.cfg" "*.cfg=tex/latex/endfloat" \
     --tdsonly "*.sty" "*.sty=tex/latex/endfloat"
then
  cp -a "$temp_dir/endfloat.tar.gz" "$dist_dir/endfloat_$(date --rfc-3339=date).tar.gz"
  cd "$dist_dir"

#  ctanupload.pl -l -p -U uktug \
#    --contribution=endfloat \
#    --name "Axel Sommerfeldt" --email "axel.sommerfeldt@f-m.fm" \
#    --summary-file "$dist_dir/SUMMARY" \
#    --directory=/macros/latex/contrib/endfloat \
#    --DoNotAnnounce=0 \
#    --license=free --freeversion=gpl \
#    --file=endfloat.tar.gz
fi

